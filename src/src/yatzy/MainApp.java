package src.yatzy;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Yatzy");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.show();
	}

	// -------------------------------------------------------------------------

	// The Yatzy game object
	private Yatzy yatzy = new Yatzy();
	// Shows the face values of the 5 dice.
	private TextField[] txfValues = new TextField[5];
	// Shows the hold status of the 5 dice.
	private CheckBox[] chbHolds = new CheckBox[5];
	private TextField[] txfResults = new TextField[15];
	private TextField txfSumSame, txfBonus, txfSumOther, txfTotal;
	private Label lblRolled;
	private Button btnRoll;
	private String[] labels = { "1-s ", "2-s ", "3-s ", "4-s ", "5-s ", "6-s ", "One pair ", "Two pairs", "Three same ",
			"Four same ", "Full house ", "Small straight ", "Large straight ", "Chance ", "Yatzy " };
	private Label[] labels2 = new Label[15];
	private boolean[] holds = { false, false, false, false, false };

	private int diceCount = 0;
	private boolean[] isTaken = new boolean[15]; // NYT!!!

	private void initContent(GridPane pane) {
		pane.setGridLinesVisible(false);
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);

		// ---------------------------------------------------------------------

		GridPane dicePane = new GridPane();
		pane.add(dicePane, 0, 0);
		dicePane.setGridLinesVisible(false);
		dicePane.setPadding(new Insets(10));
		dicePane.setHgap(10);
		dicePane.setVgap(10);
		dicePane.setStyle("-fx-border-color: black");

		// Creates the 5 boxes for the dice values
		for (int i = 0; i < txfValues.length; i++) {
			txfValues[i] = new TextField();
			txfValues[i].setPrefWidth(60);
			txfValues[i].setFont(Font.font(28));
			txfValues[i].setEditable(false);
			dicePane.add(txfValues[i], i, 0);
		}

		// Creates the 5 checkboxes used to hold the dice
		for (int i = 0; i < chbHolds.length; i++) {
			chbHolds[i] = new CheckBox("Hold");
			dicePane.add(chbHolds[i], i, 1);

			chbHolds[i].setOnAction(event -> {
				holdAction();
			});
		}

		btnRoll = new Button("Roll");
		dicePane.add(btnRoll, 3, 2);
		btnRoll.setOnAction(event -> rollDice());
		btnRoll.setDisable(false);

		lblRolled = new Label("Rolled: " + yatzy.getThrowCount());
		dicePane.add(lblRolled, 4, 2);

		// ---------------------------------------------------------------------

		GridPane scorePane = new GridPane();
		pane.add(scorePane, 0, 1);
		scorePane.setGridLinesVisible(false);
		scorePane.setPadding(new Insets(10));
		scorePane.setVgap(5);
		scorePane.setHgap(10);
		scorePane.setStyle("-fx-border-color: black");

		for (int i = 0; i < labels.length; i++) {
			labels2[i] = new Label(labels[i]);
			scorePane.add(labels2[i], 0, i);
		}

		// ----------------------------------------------------

		// Creates the 14 boxes for the possible results
		for (int i = 0; i < txfResults.length; i++) {
			txfResults[i] = new TextField();
			txfResults[i].setPrefHeight(25);
			txfResults[i].setPrefWidth(50);
			txfResults[i].setEditable(false);
			scorePane.add(txfResults[i], 1, i);
			txfResults[i].setDisable(false);
			isTaken[i] = false;
			txfResults[i].setOnMouseClicked(event -> {
				lockResultInField();
			});
		}

		// --------------------------------------------

		Label lblSumSame = new Label("Sum:");
		scorePane.add(lblSumSame, 2, 5);

		Label lblSumOther = new Label("Sum:");
		scorePane.add(lblSumOther, 2, 14);

		// --------------------------------------------

		txfSumSame = new TextField();
		txfSumSame.setPrefHeight(25);
		txfSumSame.setPrefWidth(50);
		scorePane.add(txfSumSame, 3, 5);
		txfSumSame.setEditable(false);

		txfSumOther = new TextField();
		txfSumOther.setPrefHeight(25);
		txfSumOther.setPrefWidth(50);
		scorePane.add(txfSumOther, 3, 14);
		txfSumOther.setEditable(false);

		// -------------------------------------------

		Label lblBonus = new Label("Bonus:");
		lblBonus.setStyle("-fx-font-weight: bold");
		scorePane.add(lblBonus, 4, 5);

		Label lblTotal = new Label("Total:");
		lblTotal.setStyle("-fx-font-weight: bold");
		scorePane.add(lblTotal, 4, 14);

		// --------------------------------------------

		txfBonus = new TextField();
		txfBonus.setPrefHeight(25);
		txfBonus.setPrefWidth(50);
		scorePane.add(txfBonus, 5, 5);
		txfBonus.setEditable(false);

		txfTotal = new TextField();
		txfTotal.setPrefHeight(25);
		txfTotal.setPrefWidth(50);
		scorePane.add(txfTotal, 5, 14);
		txfTotal.setEditable(false);
	}

	// -------------------------------------------------------------------------

	private void rollDice() {
		enableNonTaken();
		rollCount();
		setValueFields();
		setResultFields();
	}

	private void holdAction() {
		for (int i = 0; i < chbHolds.length; i++) {
			if (chbHolds[i].isSelected()) {
				holds[i] = true;
			} else {
				holds[i] = false;
			}
		}
	}

	private void resetHolds() {
		for (int i = 0; i < chbHolds.length; i++) {
			holds[i] = false;
			chbHolds[i].setSelected(false);
		}
	}

	private void rollCount() {
		if (yatzy.getThrowCount() >= 2) {
			btnRoll.setDisable(true);
		}
	}

	private void setValueFields() {
		yatzy.throwDice(holds);
		int[] currentRoll = yatzy.getValues();
		String diceValue;
		for (int i = 0; i < txfValues.length; i++) {
			diceValue = Integer.toString(currentRoll[i]);
			txfValues[i].setText(diceValue);
		}
		lblRolled.setText("Rolled: " + yatzy.getThrowCount());
	}

	private void setResultFields() {
		for (int i = 0; i < txfResults.length; i++) {
			if (txfResults[i].isDisabled() == false) {
				txfResults[i].setText(Integer.toString(yatzy.getPossibleResults()[i]));
			}
		}
	}

	private void clearDiceFields() {
		for (int i = 0; i < txfValues.length; i++) {
			txfValues[i].clear();
		}
	}

	// -------------------------------------------------------------------------

	private void lockResultInField() {
		for (int i = 0; i < txfResults.length; i++) {
			if (txfResults[i].isFocused() == true) {
				txfResults[i].setDisable(true);
				isTaken[i] = true;
			}
			setSumSame();
			setSumOther();
			setBonus();
			setTotal();
			resetHolds();
			btnRoll.setDisable(false);
			yatzy.resetThrowCount();
			lblRolled.setText("Rolled: " + yatzy.getThrowCount());
			clearDiceFields();
			disableAll();
		}
		gameEnd();
	}

	private void setSumSame() {
		int sum = 0;
		for (int i = 0; i < 6; i++) {
			if (isTaken[i] == true) {
				sum += Integer.parseInt(txfResults[i].getText());
			}
			txfSumSame.setText(Integer.toString(sum));
		}
	}

	private void setSumOther() {
		int sum = 0;
		for (int i = 6; i < txfResults.length; i++) {
			if (isTaken[i] == true) {
				sum += Integer.parseInt(txfResults[i].getText());
			}
			txfSumOther.setText(Integer.toString(sum));
		}
	}

	private void setBonus() {
		int sum = Integer.parseInt(txfSumSame.getText());
		int bonus = 50;
		if (sum >= 63) {
			txfBonus.setText(Integer.toString(bonus));
		} else {
			txfBonus.setText(Integer.toString(0));
		}
	}

	private void setTotal() {
		int sumSame = Integer.parseInt(txfSumSame.getText());
		int sumOther = Integer.parseInt(txfSumOther.getText());
		int bonus = Integer.parseInt(txfBonus.getText());
		int total = sumSame + sumOther + bonus;

		txfTotal.setText(Integer.toString(total));

	}

	private void gameEnd() {
		boolean hasEnded = false;

		diceCount++;

		if (diceCount == txfResults.length) {
			hasEnded = true;
		}

		if (hasEnded) {
			endAlert();
		}
	}

	private void endAlert() {
		Alert endGame = new Alert(AlertType.NONE);

		endGame.setTitle("GameOver");
		endGame.setHeaderText("Your score was: " + txfTotal.getText());
		endGame.setContentText("Do you want to play again?");

		ButtonType buttonYes = new ButtonType("Yes", ButtonData.OK_DONE);
		ButtonType buttonNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
		endGame.getButtonTypes().setAll(buttonYes, buttonNo);

		endGame.showAndWait();

		if (endGame.getResult() == buttonYes) {
			diceCount = 0;
			for (int i = 0; i < txfResults.length; i++) {
				txfResults[i].clear();
				txfResults[i].setDisable(false);
			}
			txfSumOther.clear();
			txfSumSame.clear();
			txfBonus.clear();
			txfTotal.clear();
		} else {
			System.exit(0);
		}
	}

	private void disableAll() {
		for (int i = 0; i < txfResults.length; i++) {
			if (isTaken[i] == false) {
				txfResults[i].setDisable(true);
			}
		}
	}

	private void enableNonTaken() {
		for (int i = 0; i < txfResults.length; i++) {
			if (isTaken[i] == false) {
				txfResults[i].setDisable(false);
			}
		}
	}
}
