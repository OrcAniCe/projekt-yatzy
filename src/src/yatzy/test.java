package src.yatzy;

public class test {

	public static void main(String[] args) {
		
		Yatzy yatzy = new Yatzy();
		yatzy.throwDice(new boolean[5]);
		System.out.println(yatzy.getValues());
		System.out.println("Sum of pair: " + yatzy.valueOnePair());
		System.out.println("Sum of chance: " + yatzy.valueChance());
		System.out.println("Sum of three: " + yatzy.valueThree());
		System.out.println("Sum of four: " + yatzy.valueFour());
		System.out.println("Sum of Yatzy: " + yatzy.valueYatzy());
		System.out.println("Small straight: " + yatzy.valueSmallStraight());
		System.out.println("Large Straight: " + yatzy.valueLargeStraight());
		System.out.println("Two pairs sum: " + yatzy.valueTwoPair());
		
	}

}
