package src.yatzy;

import java.util.Random;

/**
 * Models a game of Yatzy.
 */
public class Yatzy
{
    /**
     * Face values of the 5 dice. <br/>
     * 1 <= values[i] <= 6.
     */
    private int[] values = new int[5];

    /**
     * Number of times the 5 dice have been thrown. <br/>
     * 0 <= throwCount <= 3.
     */
    private int throwCount = 0;

    /**
     * Random number generator.
     */
    private Random random = new Random();

    private int[] freq = new int[7]; // nyt

    /**
     * Rolls the 5 dice. <br/>
     * Only roll dice that are not hold. <br/>
     * Requires: holds contain 5 boolean values.
     */
    public void throwDice(boolean[] holds)
    {
	int i = 0;
	// holds[i] = false;
	for (i = 0; i < 5; i++)
	{
	    if (holds[i] == false)
	    {
		int n = random.nextInt(6) + 1;
		values[i] = n;
	    }
	}
	setValues(values);
	freqFaceValue();
	throwCount++;
    }

    /**
     * Returns the number of times the five dice have been thrown.
     */
    public int getThrowCount()
    {
	return throwCount;
    }

    /**
     * Resets the throw count.
     */
    public void resetThrowCount()
    {
	throwCount = 0;
    }

    /**
     * Get current dice values
     */
    public int[] getValues()
    {
	// for(int i = 0; i < values.length - 1; i++)
	// {
	// System.out.println(values[i]);
	// }
	return values;
    }

    /**
     * Set the current dice values
     */
    public void setValues(int[] values)
    {
	this.values = values;
    }

    /**
     * Returns all results possible with the current face values. <br/>
     * The order of the results is the same as on the score board.
     */
    public int[] getPossibleResults()
    {
	int[] results = new int[15];
	for (int i = 0; i <= 5; i++)
	{
	    results[i] = this.valueSpecificFace(i + 1);
	}
	results[6] = this.valueOnePair();
	results[7] = this.valueTwoPair();
	results[8] = this.valueThree();
	results[9] = this.valueFour();
	results[10] = this.valueFullHouse();
	results[11] = this.valueSmallStraight();
	results[12] = this.valueLargeStraight();
	results[13] = this.valueChance();
	results[14] = this.valueYatzy();
	return results;
    }

    /**
     * Returns an int[7] containing the frequency of face values. <br/>
     * Frequency at index v is the number of dice with the face value v, 1 <= v
     * <= 6. <br/>
     * Index 0 is not used.
     */
    private int[] freqFaceValue()
    {
	for (int v = 1; v < freq.length; v++)
	{
	    for (int i = 0; i < 5; i++)
	    {
		if (values[i] == v)
		{
		    freq[v]++;
		}
	    }
	}
	return freq;
    }

    /**
     * Returns the total value for the dice currently showing the given face
     * value
     *
     * @param face
     *            the face value to return values for
     */
    public int valueSpecificFace(int face)
    {
	int total = 0;
	for (int i = 0; i < values.length; i++)
	{
	    if (face == values[i])
	    {
		total = total + values[i];
	    }
	}
	return total;
    }

    /**
     * Returns the maximum value for n-of-a-kind for the given n. <br/>
     * For example, valueManyOfAKind(3) returns the maximum value for 3 total =
     * total + values[i];1 <= faceValue and faceValue <= 6
     *
     * @param n
     *            number of kind
     */
    public int valueManyOfAKind(int n)
    {
	int sum = 0;
	int count = 0;
	int highestValue = 0;
	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		    highestValue = values[i];
		    if (highestValue < values[i])
		    {
			highestValue = values[i];
		    }
		}
	    }
	    if (count >= n)
	    {
		sum = highestValue * n;
	    }
	    highestValue = 0;
	    count = 0;
	}
	return sum;
    }

    /**
     * The current value if you try to score the current face values as Yatzy.
     */
    public int valueYatzy()
    {
	int sum = 0;
	int count = 0;
	int yatzy = 5;
	int highestValue = 0;
	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		    highestValue = values[i];
		    if (highestValue < values[i])
		    {
			highestValue = values[i];
		    }
		}
	    }
	    if (count == yatzy)
	    {
		sum = 50;
	    }
	    highestValue = 0;
	    count = 0;
	}
	return sum;
    }

    /**
     * Returns the current score if used as "chance".
     */
    public int valueChance()
    {
	int sum = 0;
	for (int i = 0; i < values.length; i++)
	{
	    sum = sum + values[i];
	}
	return sum;
    }

    /**
     * Returns the current score for one pair.
     */
    public int valueOnePair()
    {
	int sum = 0;
	int count = 0;
	int pair = 2;
	int highestValue = 0;
	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		    highestValue = values[i];
		    if (highestValue < values[i])
		    {
			highestValue = values[i];
		    }
		}
	    }
	    if (count >= pair)
	    {
		sum = highestValue * pair;
	    }
	    highestValue = 0;
	    count = 0;
	}
	return sum;
    }

    /**
     * Returns the current score for two pairs.
     */
    public int valueTwoPair()
    {
	int sum = 0;
	int firstPairSum = 0;
	int secondPairSum = 0;
	int currentFaceValue = 0;
	int count = 0;
	int pair = 2;
	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		    currentFaceValue = values[i];
		}
	    }
	    if (count >= pair)
	    {
		firstPairSum = currentFaceValue * pair;
	    }
	    currentFaceValue = 0;
	    count = 0;
	}

	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue && faceValue != firstPairSum / 2)
		{
		    count++;
		    currentFaceValue = values[i];
		}
	    }
	    if (count >= pair)
	    {
		secondPairSum = currentFaceValue * pair;
	    }
	    currentFaceValue = 0;
	    count = 0;
	}
	if (firstPairSum > 0 && secondPairSum > 0)
	{
	    sum = firstPairSum + secondPairSum;
	}

	return sum;
    }

    /**
     * Returns the current score for three of a kind.
     */
    public int valueThree()
    {
	int sum = 0;
	int count = 0;
	int three = 3;
	int highestValue = 0;
	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		    highestValue = values[i];
		    if (highestValue < values[i])
		    {
			highestValue = values[i];
		    }
		}
	    }
	    if (count >= three)
	    {
		sum = highestValue * three;
	    }
	    highestValue = 0;
	    count = 0;
	}
	return sum;

    }

    /**
     * Returns the current score for four of a kind.
     */
    public int valueFour()
    {
	int sum = 0;
	int count = 0;
	int four = 4;
	int highestValue = 0;
	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		    highestValue = values[i];
		    if (highestValue < values[i])
		    {
			highestValue = values[i];
		    }
		}
	    }
	    if (count >= four)
	    {
		sum = highestValue * four;
	    }
	    highestValue = 0;
	    count = 0;
	}
	return sum;
    }

    /**
     * Returns the value of a small straight with the current face values.
     */
    public int valueSmallStraight()
    {
	int sum = 0;
	int count = 0;
	int count2 = 0;
	for (int faceValue = 1; faceValue < 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		}
	    }
	    if (count == 1)
	    {
		count2++;
	    }
	    count = 0;
	    if (count2 == 5)
	    {
		sum = 15;
	    }
	}
	return sum;
    }

    /**
     * Returns the value of a large straight with the current face values.
     */
    public int valueLargeStraight()
    {
	int sum = 0;
	int count = 0;
	int count2 = 0;
	for (int faceValue = 2; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		}
	    }
	    if (count == 1)
	    {
		count2++;
	    }
	    count = 0;

	    if (count2 == 5)
	    {
		sum = 20;
	    }
	}
	return sum;
    }

    /**
     * Returns the value of a full house with the current face values.
     */
    public int valueFullHouse()
    {
	int sum = 0;
	int firstPairSum = 0;
	int secondPairSum = 0;
	int currentFaceValue = 0;
	int count = 0;
	int pair = 2;
	int threeOfAKind = 3;
	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue)
		{
		    count++;
		    currentFaceValue = values[i];
		}
	    }
	    if (count == threeOfAKind)
	    {
		firstPairSum = currentFaceValue * threeOfAKind;
	    }
	    currentFaceValue = 0;
	    count = 0;
	}

	for (int faceValue = 1; faceValue <= 6; faceValue++)
	{
	    for (int i = 0; i < values.length; i++)
	    {
		if (values[i] == faceValue && faceValue != firstPairSum / 3)
		{
		    count++;
		    currentFaceValue = values[i];
		}
	    }
	    if (count == pair)
	    {
		secondPairSum = currentFaceValue * pair;
	    }
	    currentFaceValue = 0;
	    count = 0;
	}
	if (firstPairSum > 0 && secondPairSum > 0)
	{
	    sum = firstPairSum + secondPairSum;
	}

	return sum;
    }

}
